﻿using DapperDataAccess.Test.Entities;
using TCLvcang.Base.DataAccess.Mapper;

namespace DapperDataAccess.Test.Maps
{
    public class ExternallyMappedMap
    {
        public class ExternallyMappedMapper : ClassMapper<ExternallyMapped>
        {
            public ExternallyMappedMapper()
            {
                Table("External");
                Map(x => x.Id).Column("ExternalId");
                AutoMap();
            }
        } 
    }
}
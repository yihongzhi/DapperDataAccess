﻿using System;

namespace DapperDataAccess.Test.Data
{
    class Animal
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

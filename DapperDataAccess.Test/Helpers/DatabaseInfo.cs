﻿using System.Data;
using TCLvcang.Base.DataAccess.Sql;

namespace DapperDataAccess.Test.Helpers
{
    public class DatabaseInfo
    {
        public IDbConnection Connection { get; set; }
        public ISqlDialect Dialect { get; set; }
    }
}